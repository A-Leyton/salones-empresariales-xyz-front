//#region importaciones..
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Nav
    , Navbar
    , NavDropdown
    , Jumbotron
    , Button} from 'react-bootstrap';
import RegistroUsuario from '../pages/usuario/registroUsuario';
import RegistroSalones from '../pages/salones/registroSalones';
//#endregion

//#region Constantes..
const routesJson = {
    URL: require('../routes/routes.json')
}
//#endregion

export default class app extends Component {
    //#region Funcion encargada de renderizar los componentes seleccionados.. 
    renderComponentDOM = (rutaComponent) => {
        try {
            if (rutaComponent === routesJson.URL['rutaComponent'].registroUsuario) {
                var element = <RegistroUsuario/>;
                ReactDOM.render(
                element,
                document.getElementById('root')
                );
            } else if (rutaComponent === routesJson.URL['rutaComponent'].registroSalones) {
                var element = <RegistroSalones/>;
                ReactDOM.render(
                element,
                document.getElementById('root')
                );
            }                
        } catch (error) {
            console.log("error:" + error);
        }     
    }
    //#endregion
    
    //#region Funcion encargada enviar ruta de componente e invocar la funcion renderComponentDOM..
    componentRegistroUsuario = () => {
        try {
            this.renderComponentDOM(routesJson.URL['rutaComponent'].registroUsuario)    
        } catch (error) {
            console.log("error:" + error);
        }                      
    }
    //#endregion 

    //#region Funcion encargada enviar ruta de componente e invocar la funcion renderComponentDOM..
    componentRegistroSalones = () => {
        try {
            this.renderComponentDOM(routesJson.URL['rutaComponent'].registroSalones)            
        } catch (error) {
            console.log("error:" + error);
        } 
    } 
    //#endregion
    
    render() {
        return (
            <div>
                <Navbar collapseOnSelect expand="lg" bg="success" variant="dark">
                    <Navbar.Brand href="#home">Salones Empresariales XYZ</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                      <Nav className="mr-auto">
                        <Nav.Link href="#features">Inicio</Nav.Link>
                        <NavDropdown title="Opciones" id="collasible-nav-dropdown">
                          <NavDropdown.Item href="#action/3.1">Registro salones</NavDropdown.Item>
                          <NavDropdown.Divider />
                          <NavDropdown.Item href="#action/3.4">Inicio</NavDropdown.Item>
                        </NavDropdown>
                      </Nav>           
                    </Navbar.Collapse>
                </Navbar>
                <Jumbotron>
                    <h1>Registro Usuario</h1>
                    <p>
                      Este módulo es el encargado de registrar nuevos usuarios en el sistema.
                    </p>
                    <p>
                      <Button onClick={this.componentRegistroUsuario} variant="primary">Ingresar</Button>
                    </p>
                </Jumbotron>
                <Jumbotron>
                    <h1>Registro Salones</h1>
                    <p>
                      Este módulo es el encargado de registrar nuevos salones.
                    </p>
                    <p>
                      <Button onClick={this.componentRegistroSalones} variant="primary">Ingresar</Button>
                    </p>
                </Jumbotron>
            </div>
        )
    }
}
