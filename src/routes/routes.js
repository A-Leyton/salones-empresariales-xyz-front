import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import App from '../app/app';
import RegistroUsuario from '../pages/usuario/registroUsuario';
import RegistroSalones from '../pages/salones/registroSalones';
import ModalAlert from '../components/modalAlert';

function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={App}/>
        <Route exact path="/usuario/registroUsuario" component={RegistroUsuario}/>
        <Route exact path="/salones/registroSalones" component={RegistroSalones}/>
        <Route exact path="/components/modalAlert" component={ModalAlert}/>
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;

