import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

const ModalAlert = ({ onClick, onClick2, show, title, body }) => show ?
        <div className="modal" tabindex="-1" role="dialog">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{title}</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <p>{body}</p>
          </div>
          <div className="modal-footer">
          {onClick2 ? <button className="btn btn-secondary" onClick={e => onClick2()}>Cancelar</button> : null}
            <button className="btn btn-primary" onClick={e => onClick()}>Aceptar</button>
          </div>
        </div>
      </div>
    </div>: null;

export default class modalAlert extends React.Component {
    render() {
        return (
            <div>
                {ModalAlert}
            </div>
        )
    }
}