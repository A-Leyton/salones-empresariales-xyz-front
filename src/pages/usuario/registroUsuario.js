import React from 'react';
import ReactDOM from 'react-dom';
import * as Yup from "yup";
import { FormattedMessage, injectIntl } from "react-intl";
import App from '../../app/app';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Nav
    , Navbar
    , NavDropdown
    , Jumbotron
    , Button
    , FormControl
    , InputGroup
    , Dropdown
    , ButtonGroup
    , DropdownButton} from 'react-bootstrap'
import Select from 'react-select'

const optionsCiudad = [
  { value: 1, label: 'BOGOTA D.C'}
  , { value: 2, label: 'Medellin'}
  , { value: 3, label: 'Cali'}
]

const optionsDepartamento = [
  { value: 1, label: 'BOGOTA D.C'}
  , { value: 2, label: 'Amazonas'}
  , { value: 3, label: 'Antioquia'}
]

var id="";

export default class registroUsuario extends React.Component { 
  state = {
      form: {
          "identificacion": ""
          , "nombreApellido": ""
          , "telefono": ""
          , "correo": ""
          , "id_Departamentos": ""
          , "id-ciudad": ""
          , "edad": 0
      },
      showHide: false
  }

    obtenerIdentificador=async e=>{
        await this.setState({
            form:{
                ...this.state.form,
                [e.target.id]: e.target.value                
            }
        });
    }

    precargarCiudadades = (listaResponse) => {
      try {
        if (listaResponse.length > 0) {
          for (let i = 0; i < listaResponse.length; i++) {
            this.state.form.id_Departamentos = listaResponse[i].id_Ciudad
            optionsCiudad.value = listaResponse[i].id_Ciudad
            optionsCiudad.label = listaResponse[i].ciudad
          }
        } else {
          console.log("Informacion nula.");
        }
      } catch (error) {
        console.log("error al precargar informacion ciudades." + error);
      }
    }

    validarInformacion = () => {
      const { intl } = this.props;
      const UsuarioSchema = Yup.object().shape({
        identificacion: Yup.string()
          .min(3, "Minimum 3 symbols")
          .max(50, "Maximum 50 symbols")
          .required(
            intl.formatMessage({
              id: "AUTH.VALIDATION.REQUIRED_FIELD",
            })
          ),
        nombreCompleto: Yup.string()
          .email("Wrong email format")
          .min(3, "Minimum 3 symbols")
          .max(50, "Maximum 50 symbols")
          .required(
            intl.formatMessage({
              id: "AUTH.VALIDATION.REQUIRED_FIELD",
            })
          ),
        correoElectronico: Yup.string()
          .min(3, "Minimum 3 symbols")
          .max(50, "Maximum 50 symbols")
          .required(
            intl.formatMessage({
              id: "AUTH.VALIDATION.REQUIRED_FIELD",
            })
          ),
        telefono: Yup.string()
          .min(3, "Minimum 3 symbols")
          .max(50, "Maximum 50 symbols")
          .required(
            intl.formatMessage({
              id: "AUTH.VALIDATION.REQUIRED_FIELD",
            })
          )
      }); 
    }

    initCiudad = () => {
      try {
        this.getCiudad();
      } catch (error) {
        console.log("error al iniciar servicios:" + error)
      }       
    }

  getDepartamentos = ()  => {
      setTimeout(() => {
        const URL_BASE_API = require('../../api/urlApi.json');        
        const requestOptions = {
          method: 'GET',
          headers: { 'Content-Type': 'application/json' }
        };

      fetch(URL_BASE_API.urlBase.departamentos, requestOptions)
        .then(async response => {
          const data = await response.json();
          if (!response.ok) {
            const error = (data && data.message) || response.status;
            return Promise.reject(error);
          } else {
            console.log(response);
            alert("registro exitoso");
            this.renderAppDOM();        
          }            
          })
          .catch((error) => {
            console.log("error al consumir servicio:" + error);
          });
      }, 1000);
  }

  getCiudad = ()  => {
    setTimeout(() => {
      const URL_BASE_API = require('../../api/urlApi.json');        
      const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      };

    fetch(URL_BASE_API.urlBase.ciudades, requestOptions)
      .then(async response => {
        const data = await response.json();
        if (!response.ok) {
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
        } else {
          this.precargarCiudadades(data);
        }            
        })
        .catch((error) => {
          console.log("error al consumir servicio:" + error);
        });
    }, 1000);
  }

  getMotivo = ()  => {
    setTimeout(() => {
      const URL_BASE_API = require('../../api/urlApi.json');        
      const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      };

    fetch(URL_BASE_API.urlBase.motivo, requestOptions)
      .then(async response => {
        const data = await response.json();
        if (!response.ok) {
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
        } else {
          console.log(response);
          alert("registro exitoso");
          this.renderAppDOM();        
        }            
        })
        .catch((error) => {
          console.log("error al consumir servicio:" + error);
        });
    }, 1000);
  }

  guardarInformacion = () => {
      this.postRegistroUsuario();
  }

    postRegistroUsuario = () => {
      var fecha_registro = new Date();
      setTimeout(() => {
        const URL_BASE_API = require('../../api/urlApi.json');        
        const requestOptions = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ identificacion: this.state.form.identificacion.toString()
          , nombreApellidos: this.state.form.nombreApellidos.toString()
          , correo: this.state.form.correo.toString()
          , telefono: this.state.form.telefono.toString()
          , id_Departamentos: 1
          , id_Ciudad: 1
          , edad: parseInt(this.state.form.edad)
          , fechaRegistro: "2021-05-22T02:47:04.43"
          , estado: 1})
        };

      fetch(URL_BASE_API.urlBase.registroUsuario, requestOptions)
        .then(async response => {
          const data = await response.json();
          if (!response.ok) {
            const error = (data && data.message) || response.status;
            return Promise.reject(error);
          } else {
            alert("registro exitoso");
            this.renderAppDOM();        
          }            
          })
          .catch((error) => {
            console.log("error al consumir servicio:" + error);
          });
      }, 1000);
    }

    renderAppDOM = () => {
      var element = <App/>;
        ReactDOM.render(
        element,
        document.getElementById('root')
      );
    }
    
    render() {
        return (
            <div>
                <Navbar collapseOnSelect expand="lg" bg="success" variant="dark">
                    <Navbar.Brand href="#home">Salones Empresariales XYZ</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                      <Nav className="mr-auto">
                        <Nav.Link href="#features">Inicio</Nav.Link>
                        <NavDropdown title="Opciones" id="collasible-nav-dropdown">
                          <NavDropdown.Item href="#action/3.1">Registro salones</NavDropdown.Item>
                          <NavDropdown.Divider />
                          <NavDropdown.Item href="#action/3.4">Inicio</NavDropdown.Item>
                        </NavDropdown>
                      </Nav>           
                    </Navbar.Collapse>
                </Navbar>
                <Jumbotron>
                    <h3>Registro Usuario</h3>
                    <br/>
                    <label htmlFor="basic-url">Identificación</label>
                    <InputGroup className="col-md-4">
                      <InputGroup.Prepend>
                        <InputGroup.Text>n.</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        id={"identificacion"}
                        placeholder="Ingrese número de identificación"
                        onChange={this.obtenerIdentificador}
                      />
                    </InputGroup>
                    
                    <label htmlFor="basic-url">Nombre y Apellido</label>
                    <InputGroup className="col-md-4">
                      <InputGroup.Prepend>
                        <InputGroup.Text>n-a</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        id={"nombreApellidos"}
                        placeholder="Ingrese nombre y apellidos"
                        onChange={this.obtenerIdentificador}
                      />
                    </InputGroup>

                    <label htmlFor="basic-url">Teléfono</label>
                    <InputGroup className="col-md-4">
                      <InputGroup.Prepend>
                        <InputGroup.Text>#</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        id={"telefono"}
                        placeholder="Ingrese número de telefono"
                        onChange={this.obtenerIdentificador}
                      />
                    </InputGroup>

                    <label htmlFor="basic-url">Correo electronico</label>
                    <InputGroup className="col-md-4">
                      <InputGroup.Prepend>
                        <InputGroup.Text>@</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        id={"correo"}
                        placeholder="Ingrese correo"
                        onChange={this.obtenerIdentificador}
                      />
                    </InputGroup>

                    <label htmlFor="basic-url">Departamentos</label>
                    <Select className={"col-md-3"} options={optionsDepartamento}>
                    </Select>

                      <label htmlFor="basic-url">Ciudad</label>
                      <Select className={"col-md-3"} options={optionsCiudad}>
                      </Select>   
                      
                      <label htmlFor="basic-url">Edad</label>
                      <InputGroup className="col-md-4">
                      <InputGroup.Prepend>
                        <InputGroup.Text>@</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        id={"edad"}
                        placeholder="Ingrese edad"
                        onChange={this.obtenerIdentificador}
                      />
                    </InputGroup>

                    <br/>                    
                    <p>
                      <Button onClick={this.guardarInformacion} variant="primary">Guadar</Button>
                    </p>                            
                </Jumbotron>
                <div>
            </div>
          </div>
        )
    }
}
